'use strict';
var syntax        = 'scss', // Syntax: sass or scss;
    gulpversion   = '4'; // Gulp version: 3 or 4

var gulp          = require('gulp'),
    gutil         = require('gulp-util' ),
    sass          = require('gulp-sass'),
    browserSync   = require('browser-sync'),
    babel = require('gulp-babel'),//занимается преобразованием ES6 в ES5
    filesize = require('gulp-filesize'),//выводит размер файла
    concat        = require('gulp-concat'),
    uglify        = require('gulp-uglify'),
    cleancss      = require('gulp-clean-css'),
    rename        = require('gulp-rename'),
    autoprefixer  = require('gulp-autoprefixer'),
    notify        = require('gulp-notify'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber'),
    sourcemaps  = require('gulp-sourcemaps'),
    fileinclude = require('gulp-file-include'),
    rsync         = require('gulp-rsync');


gulp.task('fileinclude', async function() {
    gulp.src(['app/src/*.php'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file',
        }))
        .on("error", notify.onError())
        .pipe(gulp.dest('app/'));
});

gulp.task('code', function() {
    return gulp.src('**/*.php')
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "code",
        notify: false,
        port: 7000,
        // open: false,
        // online: false, // Work Offline Without Internet Connection
        // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
        files: [
            '**/*.php',
            // '.htaccess'
        ]
    });
});

const path = {
    'scssScreen': [
        'app/styles/**/*.'+syntax+'',
        'app/blocks/**/*.'+syntax+''],
    'scriptScreen': [
        'app/blocks/**/*.js'],
    'includeScreen': [
        'app/src/*.php',
        'app/blocks/**/*.php'
    ]
};

// Конкатенация и минификация файлов JS
gulp.task('js', async function(){
    gulp.src(path.scriptScreen)
    // .pipe(plugins.fixmyjs(
    //     configFile: '.eslintrc.json'
    // ))
    // .pipe(eslint({configFile: '.eslintrc.json'}))
    // .pipe(eslint())
    // .pipe(eslint.format())
    // .pipe(eslint.failAfterError())
        .pipe(filesize())
        .on("error", notify.onError()) // handle error for eslint
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .on("error", notify.onError()) // handle error for babel
        .pipe(concat('compiled.js'))
        .pipe(rename('compiled.min.js'))
        .pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(browserSync.stream())
        .pipe(gulp.dest('app/js/'))
        .pipe(filesize());
});

gulp.task('styles', function() {
    return gulp.src(path.scssScreen)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/css'))
        .pipe(rename({ suffix: '.min', prefix : '' }))
        //.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging

        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream())
});

gulp.task('imgMin', function () {
    return gulp.src('app/img/**/*.{png,jpg,svg}').pipe(imagemin([
        imagemin.optipng({optimizationLevel: 3}),
        imagemin.jpegtran({progressive: true}),
        imagemin.svgo()
    ])).pipe(gulp.dest('app/img'));
});


gulp.task('rsync', function() {
    return gulp.src('app/**')
        .pipe(rsync({
            root: 'app/',
            hostname: 'username@yousite.com',
            destination: 'yousite/public_php/',
            // include: ['*.htaccess'], // Includes files to deploy
            exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
});

if (gulpversion == 3) {
    gulp.task('watch', ['styles', 'imgMin', 'browser-sync'], function() {
        gulp.watch('app/'+syntax+'/**/*.'+syntax+'', ['styles']);
        gulp.watch(['libs/**/*.js', 'app/js/common.js']);
    });
    gulp.task('default', ['watch']);
}

if (gulpversion == 4) {
    gulp.task('watch', function() {
        gulp.watch(path.scssScreen, gulp.parallel('styles'));
        // gulp.watch('**/*.php', gulp.parallel('code'));
        gulp.watch(path.includeScreen, gulp.parallel('fileinclude'));
        gulp.watch(path.scriptScreen, gulp.parallel('js'));
        // gulp.watch('app/blocks/**/*.'+syntax+'', gulp.parallel('styles-module'));

        // gulp.watch('app/js/*.js', gulp.parallel('code'));
    });
    gulp.task('default', gulp.parallel( 'imgMin', 'browser-sync', 'js', 'watch'));
}