<?
/*********************************
 * Convertation of HTML to text
 *********************************/
function HTMLToTxt($str, $strSiteUrl = "", $aDelete = array(), $maxlen = 70)
{
    //get rid of whitespace
    $str = preg_replace("/[\\t\\n\\r]/", " ", $str);

    //replace tags with placeholders
    static $search = array(
        "'<script[^>]*?>.*?</script>'si",
        "'<style[^>]*?>.*?</style>'si",
        "'<select[^>]*?>.*?</select>'si",
        "'&(quot|#34);'i",
        "'&(iexcl|#161);'i",
        "'&(cent|#162);'i",
        "'&(pound|#163);'i",
        "'&(copy|#169);'i",
    );

    static $replace = array(
        "",
        "",
        "",
        "\"",
        "\xa1",
        "\xa2",
        "\xa3",
        "\xa9",
    );

    $str = preg_replace($search, $replace, $str);

    $str = preg_replace("#<[/]{0,1}(b|i|u|em|small|strong)>#i", "", $str);
    $str = preg_replace("#<[/]{0,1}(font|div|span)[^>]*>#i", "", $str);

    //���� ������
    $str = preg_replace("#<ul[^>]*>#i", "\r\n", $str);
    $str = preg_replace("#<li[^>]*>#i", "\r\n  - ", $str);

    //������ �� ��� �������
    foreach ($aDelete as $del_reg)
        $str = preg_replace($del_reg, "", $str);

    //���� ��������
    $str = preg_replace("/(<img\\s.*?src\\s*=\\s*)([\"']?)(\\/.*?)(\\2)(\\s.+?>|\\s*>)/is", "[" . chr(1) . $strSiteUrl . "\\3" . chr(1) . "] ", $str);
    $str = preg_replace("/(<img\\s.*?src\\s*=\\s*)([\"']?)(.*?)(\\2)(\\s.+?>|\\s*>)/is", "[" . chr(1) . "\\3" . chr(1) . "] ", $str);

    //���� ������
    $str = preg_replace("/(<a\\s.*?href\\s*=\\s*)([\"']?)(\\/.*?)(\\2)(.*?>)(.*?)<\\/a>/is", "\\6 [" . chr(1) . $strSiteUrl . "\\3" . chr(1) . "] ", $str);
    $str = preg_replace("/(<a\\s.*?href\\s*=\\s*)([\"']?)(.*?)(\\2)(.*?>)(.*?)<\\/a>/is", "\\6 [" . chr(1) . "\\3" . chr(1) . "] ", $str);

    //���� <br>
    $str = preg_replace("#<br[^>]*>#i", "\r\n", $str);

    //���� <p>
    $str = preg_replace("#<p[^>]*>#i", "\r\n\r\n", $str);

    //���� <hr>
    $str = preg_replace("#<hr[^>]*>#i", "\r\n----------------------\r\n", $str);

    //���� �������
    $str = preg_replace("#<[/]{0,1}(thead|tbody)[^>]*>#i", "", $str);
    $str = preg_replace("#<([/]{0,1})th[^>]*>#i", "<\\1td>", $str);

    $str = preg_replace("#</td>#i", "\t", $str);
    $str = preg_replace("#</tr>#i", "\r\n", $str);
    $str = preg_replace("#<table[^>]*>#i", "\r\n", $str);

    $str = preg_replace("#\r\n[ ]+#", "\r\n", $str);

    //����� ������ ��� ���������� ����
    $str = preg_replace("#<[/]{0,1}[^>]+>#i", "", $str);

    $str = preg_replace("#[ ]+ #", " ", $str);
    $str = str_replace("\t", "    ", $str);

    //��������� ������� ������
    if ($maxlen > 0)
        $str = preg_replace("#([^\\n\\r]{" . intval($maxlen) . "}[^ \\r\\n]*[\\] ])([^\\r])#", "\\1\r\n\\2", $str);

    $str = str_replace(chr(1), " ", $str);
    return trim($str);
}

function FormatText($strText, $strTextType = "text")
{
    if (strtolower($strTextType) == "html")
        return $strText;

    return TxtToHtml($strText);
}

function htmlspecialcharsEx($str)
{
    static $search = array("&amp;", "&lt;", "&gt;", "&quot;", "&#34", "&#x22", "&#39", "&#x27", "<", ">", "\"");
    static $replace = array("&amp;amp;", "&amp;lt;", "&amp;gt;", "&amp;quot;", "&amp;#34", "&amp;#x22", "&amp;#39", "&amp;#x27", "&lt;", "&gt;", "&quot;");
    return str_replace($search, $replace, $str);
}

function htmlspecialcharsback($str)
{
    static $search = array("&lt;", "&gt;", "&quot;", "&apos;", "&amp;");
    static $replace = array("<", ">", "\"", "'", "&");
    return str_replace($search, $replace, $str);
}

function htmlspecialcharsbx($string, $flags = ENT_COMPAT)
{
    //shitty function for php 5.4 where default encoding is UTF-8
    return htmlspecialchars($string, $flags, (defined("BX_UTF") ? "UTF-8" : "ISO-8859-1"));
}

function TxtToHTML(
    $str,                                    // ����� ��� ��������������
    $bMakeUrls = true,           // true - ��������������� URL � <a href="URL">URL</a>
    $iMaxStringLen = 0,              // ������������ ����� ����� ��� �������� ��� �������� �������� �������
    $QUOTE_ENABLED = "N",            // Y - ������������� <QUOTE>...</QUOTE> � ����� ������
    $NOT_CONVERT_AMPERSAND = "Y",            // Y - �� ��������������� ������ "&" � "&amp;"
    $CODE_ENABLED = "N",            // Y - ������������� <CODE>...</CODE> � readonly textarea
    $BIU_ENABLED = "N",            // Y - ������������� <B>...</B> � �.�. � ��������������� HTML ����
    $quote_table_class = "quotetable",   // css ����� �� ������� ������
    $quote_head_class = "tdquotehead",  // css ����� �� ������ TD ������� ������
    $quote_body_class = "tdquote",      // css ����� �� ������ TD ������� ������
    $code_table_class = "codetable",    // css ����� �� ������� ����
    $code_head_class = "tdcodehead",   // css ����� �� ������ TD ������� ����
    $code_body_class = "tdcodebody",   // css ����� �� ������ TD ������� ����
    $code_textarea_class = "codetextarea", // css ����� �� textarea � ������� ����
    $link_class = "txttohtmllink",// css ����� �� �������
    $arUrlEvent = array(),        // ������ � ��� ���� ������ ����� EVENT1, EVENT2, EVENT3 �� ������ ����� ����� $arUrlEvent["SCRIPT"] (�� ��������� ����� "/bitrix/redirect.php")
    $link_target = "_self"         // tagret �������� ��������
)
{
    global $QUOTE_ERROR, $QUOTE_OPENED, $QUOTE_CLOSED;
    $QUOTE_ERROR = $QUOTE_OPENED = $QUOTE_CLOSED = 0;

    $str = delete_special_symbols($str);

    // ������� ���������� chr(2) ��� ��� � ���������� ���������� �������� ������
    if ($iMaxStringLen > 0)
        $str = InsertSpaces($str, $iMaxStringLen, chr(2), true);

    // \ => chr(8)
    $str = str_replace("\\", chr(8), $str); // ���������� ���������� ���� "\"

    // <quote>...</quote> => [quote]...[/quote]
    if ($QUOTE_ENABLED == "Y")
        $str = preg_replace("#(?:<|\\[)(/?)quote(.*?)(?:>|\\])#is", " [\\1quote]", $str);

    // <code>...</code> => [code]...[/code]
    // \n => chr(4)
    // \r => chr(5)
    if ($CODE_ENABLED == "Y") {
        $helper = new CConvertorsPregReplaceHelper("");
        $str = preg_replace("#<code(\\s+[^>]*>|>)(.+?)</code(\\s+[^>]*>|>)#is", "[code]\\2[/code]", $str);
        $str = preg_replace_callback("#\\[code(\\s+[^\\]]*\\]|\\])(.+?)\\[/code(\\s+[^\\]]*\\]|\\])#is", array($helper, "convertCodeTagForHtmlBefore"), $str);
    }

    // <b>...</b> => [b]...[/b]
    // <i>...</i> => [i]...[/i]
    // <u>...</u> => [u]...[/u]
    if ($BIU_ENABLED == "Y") {
        $str = preg_replace("#<b(\\s+[^>]*>|>)(.+?)</b(\\s+[^>]*>|>)#is", "[b]\\2[/b]", $str);
        $str = preg_replace("#<i(\\s+[^>]*>|>)(.+?)</i(\\s+[^>]*>|>)#is", "[i]\\2[/i]", $str);
        $str = preg_replace("#<u(\\s+[^>]*>|>)(.+?)</u(\\s+[^>]*>|>)#is", "[u]\\2[/u]", $str);
    }

    // URL => chr(1).URL."/".chr(1)
    // EMail => chr(3).E-Mail.chr(3)
    if ($bMakeUrls) {
        //hide @ from next regexp with chr(11)
        $str = preg_replace_callback("#((http|https|ftp):\\/\\/[a-z:@,.'/\\#\\%=~\\&?*+\\[\\]_0-9\x01-\x08-]+)#is", array("CConvertorsPregReplaceHelper", "extractUrl"), $str);
        $str = preg_replace("#(([=_\\.'0-9a-z+~\x01-\x08-]+)@[_0-9a-z\x01-\x08-.]+\\.[a-z]{2,10})#is", chr(3) . "\\1" . chr(3), $str);
        //replace back to @
        $str = str_replace(chr(11), '@', $str);
    }

    // ����������� ��������� ��������
    if ($NOT_CONVERT_AMPERSAND != "Y") $str = str_replace("&", "&amp;", $str);
    static $search = array("<", ">", "\"", "'", "%", ")", "(", "+");
    static $replace = array("&lt;", "&gt;", "&quot;", "&#39;", "&#37;", "&#41;", "&#40;", "&#43;");
    $str = str_replace($search, $replace, $str);

    // chr(1).URL."/".chr(1) => <a href="URL">URL</a>
    // chr(3).E-Mail.chr(3) => <a href="mailto:E-Mail">E-Mail</a>
    if ($bMakeUrls) {
        $script = $arUrlEvent["SCRIPT"];
        $helper = new CConvertorsPregReplaceHelper("");
        $helper->setLinkClass($link_class);
        $helper->setLinkTarget($link_target);
        $helper->setEvents($arUrlEvent["EVENT1"], $arUrlEvent["EVENT2"], $arUrlEvent["EVENT3"]);
        if (strlen($script))
            $helper->setScript($script);
        $str = preg_replace_callback("#\x01([^\n\x01]+?)/\x01#is", array($helper, "convertToHref"), $str);
        $str = preg_replace_callback("#\x03([^\n\x03]+?)\x03#is", array($helper, "convertToMailTo"), $str);
    }

    $str = str_replace("\r\n", "\n", $str);
    $str = str_replace("\n", "<br />\n", $str);
    $str = preg_replace("# {2}#", "&nbsp;&nbsp;", $str);
    $str = preg_replace("#\t#", "&nbsp;&nbsp;&nbsp;&nbsp;", $str);

    // chr(2) => " "
    if ($iMaxStringLen > 0)
        $str = str_replace(chr(2), "<wbr>", $str);

    // [quote]...[/quote] => <table>...</table>
    if ($QUOTE_ENABLED == "Y") {
        $helper = new CConvertorsPregReplaceHelper("");
        $helper->setQuoteClasses($quote_table_class, $quote_head_class, $quote_body_class);
        $str = preg_replace_callback("#(\\[quote(.*?)\\](.*)\\[/quote(.*?)\\])#is", array($helper, "convertQuoteTag"), $str);
    }

    // [code]...[/code] => <textarea>...</textarea>
    // chr(4) => \n
    // chr(5) => \r
    if ($CODE_ENABLED == "Y") {
        $helper = new CConvertorsPregReplaceHelper("");
        $helper->setCodeClasses($code_table_class, $code_head_class, $code_body_class, $code_textarea_class);
        $str = preg_replace_callback("#\\[code\\](.*?)\\[/code\\]#is", array($helper, "convertCodeTagForHtmlAfter"), $str);
        $str = str_replace(chr(4), "\n", $str);
        $str = str_replace(chr(5), "\r", $str);
        $str = str_replace(chr(6), " ", $str);
        $str = str_replace(chr(7), "\t", $str);
        $str = str_replace(chr(16), "[", $str);
        $str = str_replace(chr(17), "]", $str);
    }

    // [b]...[/b] => <b>...</b>
    // [i]...[/i] => <i>...</i>
    // [u]...[/u] => <u>...</u>
    if ($BIU_ENABLED == "Y") {
        $str = preg_replace("#\\[b\\](.*?)\\[/b\\]#is", "<b>\\1</b>", $str);
        $str = preg_replace("#\\[i\\](.*?)\\[/i\\]#is", "<i>\\1</i>", $str);
        $str = preg_replace("#\\[u\\](.*?)\\[/u\\]#is", "<u>\\1</u>", $str);
    }

    // chr(8) => \
    $str = str_replace(chr(8), "\\", $str);

    $str = delete_special_symbols($str);

    return $str;
}

/*********************************
 * Convertation of HTML to text
 *********************************/
function declOfNum($num, $titles)
{
    $cases = array(2, 0, 1, 1, 1, 2);
    return $num . " " . $titles[($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)]];
}

?>
<?php
$json = [];

$type = intval($_POST["type"]);
$actv = htmlspecialcharsEx(trim($_POST["actv"]));
$balls = intval($_POST["balls"]);

if (!empty($type)) {
    $json['type'] = $type;
    $json['actv'] = $actv;
    $json['balls_val'] = declOfNum($balls, ["балл", "балла", "баллов"]);
    $json['ok'] = true;
    $json['message'] = 'Форма успешно сохранена!';
} else {
    $json['ok'] = false;
    $json['message'] = 'Не удалось сохранить форму!';
}

echo json_encode($json);
die();
?>
<?/*
<script>
$(document).on('submit', '.js-form', function () {

        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var $btn = $(this).find('[type=submit]');
        var data = $(this).serialize();
        var $form = $(this);
        var $title = $form.find('h3');
        var actvName = encodeURI($title.text());


        data += '&actv=' + actvName;
        $btn.attr('disabled', true);
        $.ajax({
            type: type,
            url: action,
            dataType: 'json',
            data: data,
            success: function (result) {
                if (result.ok) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: result.message,
                        showConfirmButton: true,
                        timer: 2000,
                        customClass: {
                            popup: 'succes-modal'
                        }
                    });
                    if (flag) {
                        $form.find('input').removeClass('notNull');
                    }

                    var card = $(document).find('.active-modal');
                    actvName = card.find('.card-actv__title'),//заголовок
                        actvNameId = card.find('.card-actv__title').attr('data-id'),//идишник события
                        balls_count = card.find('.balls-block__count'),//ссылка
                        balls_val = card.find('.balls-block__units'),//ссылка

                    actvName.text(result.actv);
                    balls_val.text(result.balls_val);
                    balls_count.text(result.balls_count);
                    actvName.attr('data-id', result.type)

                } else {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: result.message,
                        timer: 2000,
                        showConfirmButton: true,
                        customClass: {
                            popup: 'succes-modal'
                        }

                    });

                }
                $btn.attr('disabled', false);
            }
        });
        return false;
    });
</script>
*/?>
<?/*
<form action="/activities/" method="post" class="js-form" data-id="45">
	<input type="hidden" name="save" value="1">
	<div class="form-group">
		<p class="form-title">Тип активности</p>
		<div class="form-label">
			<div class="custom-select" style="width:200px;">
		<select required="" name="type">
			<option value="10">Статьи и обзоры в газетах, журналах/ участие в радиопередачах или на ТВ</option><option value="11">Статьи в интернет изданиях</option><option value="13">Участие в качестве Бренд-эксперта на выставке, конференции, форуме (не онлайн-мероприятие)</option><option value="14">Фото и видео в соц.сетях (обязательно наличие плашки бренда и текста в посте)</option><option value="15">Обзор продуктов BrowXenna (фото с обзором или видеообзоры)</option><option value="16">Положительный обзор на новый продукт бреда в течение 2-х месяцев со дня старта продаж продукта</option><option value="19">Приведенный в сообщество бровист</option>		</select>
	<div class="select-selected"><span>Статьи и обзоры в газетах, журналах/ участие в радиопередачах или на ТВ</span></div><div class="select-items select-hide"><div><span>Статьи и обзоры в газетах, журналах/ участие в радиопередачах или на ТВ</span></div><div><span>Статьи в интернет изданиях</span></div><div><span>Участие в качестве Бренд-эксперта на выставке, конференции, форуме (не онлайн-мероприятие)</span></div><div><span>Фото и видео в соц.сетях (обязательно наличие плашки бренда и текста в посте)</span></div><div><span>Обзор продуктов BrowXenna (фото с обзором или видеообзоры)</span></div><div><span>Положительный обзор на новый продукт бреда в течение 2-х месяцев со дня старта продаж продукта</span></div><div><span>Приведенный в сообщество бровист</span></div></div></div>
		</div>
	</div>
	<div class="form-group">
		<p class="form-title">Комментарий к активности</p>
		<div class="form-label">
			<textarea required="" autoresize="" placeholder="Опишите подробно свое участие в активности" class="textarea" name="comment" cols="15" rows="10"></textarea>
		</div>
	</div>
	<div class="form-group">
		<p class="form-title">Ссылка</p>
		<div class="form-label">
			<input class="input activity__link" required="" type="text" name="link" placeholder="Ссылка на подтверждение активности, например пост в инстаграм">
		</div>
	</div>
	<div class="form-group form-group--hidden">
		<div class="profile__row">
			<div class="profile__column">
				<label class="form-label">
					<input class=" name profile__input input" name="kandidat_name" required="" type="text">
					<span class="label-name">Имя кандидата</span>
				</label>
				<label class="form-label">
					<input class="surname profile__input input" name="kandidat_lastname" required="" type="text">
					<span class="label-name">Фамилия кандидата</span>
				</label>
			</div>
			<div class="profile__column">
				<label class="form-label">
					<input class="profile__input input" name="kandidat_inst" required="" type="text">
					<span class="label-name">Инстаграмм кандидата</span>
				</label>
				<label class="form-label">
					<input class="profile__input input phone-key" name="kandidat_phone" required="" type="text">
					<span class="label-name">Телефон кандидата</span>
				</label>
			</div>
		</div>
	</div>
	<div class="activity__btn">
		<button type="submit" class="btn btn--big waves-effect">Сохранить</button>
	</div>
</form>
*/?>
